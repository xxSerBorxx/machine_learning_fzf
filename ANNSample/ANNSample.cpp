#include <ANN.h>
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");
	
	string pathFileData = "../Debug/xorData.txt";
	string pathFileSave = "../Debug/xorSave.txt";
	
	cout << "������, ANN!" << endl;
	cout << ANN::GetTestString().c_str() << endl;

	vector<size_t> countNeironLayer = { 2, 10, 10, 1 };
	shared_ptr<ANN::ANeuralNetwork> neuralNetwork = ANN::CreateNeuralNetwork(countNeironLayer);

	cout << "\n������ ��������� ���� �� �����..." << endl;
	bool isLoad = neuralNetwork->Load(pathFileSave);
	cout << ((isLoad) ? "������ ������ �������!" : "������ ������ ��������!") << endl;

	cout << "\n���������� �� ��������� ����:\n";
	string infoNeuralNetwork = neuralNetwork->GetType();
	cout << infoNeuralNetwork.c_str() << endl;

	vector<vector<float, allocator<float>>> inputs, outputs;
	cout << "\n������ �������� ������..." << endl;
	bool isLoadData = ANN::LoadData(pathFileData, inputs, outputs);
	cout << (isLoad ? "������ ������ �������!" : "������ ������ ��������!") << endl;

	cout << "\n���������:" << endl;
	for (size_t i = 0; i < inputs.size(); i++)
	{
		vector<float> output = neuralNetwork->Predict(inputs[i]);
		for (size_t j = 0; j < output.size(); j++)
			cout << inputs[i][0] << "\t" << inputs[i][1] << "\t" << outputs[i][0] << "\t" << output[j] << endl;
	}

	return 0;
}