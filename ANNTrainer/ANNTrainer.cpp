#include <ANN.h>
#include <iostream>
using namespace std;

int main()
{
	setlocale(LC_ALL, "Russian");

	string pathFileData = "../Debug/xorData.txt";
	string pathFileSave = "../Debug/xorSave.txt";
	vector<vector<float, allocator<float>>> inputs, outputs;

	cout << "������, ANN!" << endl;
	cout << ANN::GetTestString().c_str() << endl;

	bool isLoad = ANN::LoadData(pathFileData, inputs, outputs);
	cout << "\n������ ��������� ������..." << endl;
	cout << (isLoad ? "������ ������ �������!" : "������ ������ ��������!") << endl;

	cout << "\n�������� ��������� ����:\n" << "������� ���� - 2 �������; �������� - 1 ������" << endl;
	cout << "���������� ������� ���� 2 - � ������ �� 10 ��������" << endl;
	vector<size_t> countNeironLayer = { 2, 10, 10, 1 };
	shared_ptr<ANN::ANeuralNetwork> neuralNetwork = ANN::CreateNeuralNetwork(countNeironLayer);

	cout << "\n�������� ��������� ����:\n";
	cout << "��������...\n";
	BackPropTraining(neuralNetwork, inputs, outputs, 100000, 0.1f, 0.1f, true);
	cout << "�������� ���������!" << endl;

	cout << "\n���������� �� ��������� ����:\n";
	string infoNeuralNetwork = neuralNetwork->GetType();
	cout << infoNeuralNetwork.c_str() << endl;

	cout << "\n���������� ��������� ���� � ����...\n";
	bool succeedSave = neuralNetwork->Save(pathFileSave);
	cout << (succeedSave ? "���������� �������!" : "���������� ��������!") << endl;

	return 0;
}