#include <iostream>
#include <FeatureExtraction.h>

using namespace std;
using namespace cv;

int main()
{
	cout << "hello world!" << endl;
	cout << fe::GetTestString().c_str() << endl;

	vector<Mat> result, normalizeResult;
	vector<vector<Point>> contours;
	vector<Vec4i> hierarhy;

	string pathPicture = "../Debug/numbers.png";
	Mat image = imread(pathPicture, CV_LOAD_IMAGE_GRAYSCALE);
	imshow("Numbers", image);
	waitKey();
		
	threshold(image, image, 127, 255, THRESH_BINARY_INV);
	findContours(image, contours, hierarhy, RETR_TREE, CHAIN_APPROX_SIMPLE);

	for (int idx = 0; idx >= 0; idx = hierarhy[idx][0])
	{
		Point2f center;
		float radius;
		minEnclosingCircle(contours[idx], center, radius);

		result.push_back(Mat::zeros((int)(radius * 2), (int)(radius * 2), CV_8UC1));
		Point2f offset = Point2f(radius, radius) - center;
		drawContours(result.back(), contours, idx, (255, 255, 255), CV_FILLED, LINE_8, hierarhy, 255, offset);
	}

	for (int i = 0; i < result.size(); i++)
	{
		Mat normSymbol;
		// M����������.
		resize(result[i], normSymbol, Size(50, 50));
		normalizeResult.push_back(normSymbol);
	}

	for (int i = 0; i < result.size(); i++)
		imshow("Wnd" + i, normalizeResult[i]);
	waitKey(0);

	return 0;
}